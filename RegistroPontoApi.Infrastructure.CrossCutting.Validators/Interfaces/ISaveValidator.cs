namespace RegistroPontoApi.Infrastructure.CrossCutting.Validators.Interfaces
{
    public interface ISaveValidator<in TDomainModel> : IValidator<TDomainModel>
    {
    }
}