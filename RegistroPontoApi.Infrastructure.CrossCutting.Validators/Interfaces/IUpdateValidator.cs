namespace RegistroPontoApi.Infrastructure.CrossCutting.Validators.Interfaces
{
    public interface IUpdateValidator<in TDomainModel> : IValidator<TDomainModel>
    {
    }
}