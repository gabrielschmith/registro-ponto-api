using FluentValidation;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroPontoApi.Infrastructure.CrossCutting.Validators;
using RegistroPontoApi.Infrastructure.CrossCutting.Validators.Interfaces;

namespace RegistroPontoApi.Services.Validators
{
    public class CollaboratorPointRecordValidator : Validator<CollaboratorPointRecord>,
        ISaveValidator<CollaboratorPointRecord>,
        IUpdateValidator<CollaboratorPointRecord>
    {
        public CollaboratorPointRecordValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.CollaboratorId)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.PointDateTime)
                .NotNull();
            RuleFor(x => x.Operation)
                .NotNull()
                .NotEmpty()
                .Must(c => c == 'E' || c == 'S');
        }
    }
}