using FluentValidation;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroPontoApi.Infrastructure.CrossCutting.Validators;
using RegistroPontoApi.Infrastructure.CrossCutting.Validators.Interfaces;

namespace RegistroPontoApi.Services.Validators
{
    public class CollaboratorValidator : Validator<Collaborator>,
        ISaveValidator<Collaborator>,
        IUpdateValidator<Collaborator>
    {
        public CollaboratorValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .MaximumLength(100);
            RuleFor(x => x.Username)
                .NotNull()
                .NotEmpty()
                .MaximumLength(50);
            RuleFor(x => x.Password)
                .NotNull()
                .NotEmpty()
                .MaximumLength(100);
            RuleFor(x => x.Role)
                .NotNull()
                .NotEmpty()
                .MaximumLength(100);
        }
    }
}