using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Services.Interfaces;

namespace RegistroPontoApi.Services.Services
{
    public abstract class BaseService<TDomainModel, TDomainId> : IBaseService<TDomainModel, TDomainId>
        where TDomainModel : class, TDomainId
    {
        protected readonly IReadWriteRepository<TDomainModel, TDomainId> ReadWriteRepository;

        protected readonly IServiceValidator<TDomainModel> Validator;

        protected BaseService(IReadWriteRepository<TDomainModel, TDomainId> readWriteRepository,
            IServiceValidator<TDomainModel> validator)
        {
            ReadWriteRepository = readWriteRepository;
            Validator = validator;
        }

        public async ValueTask<TDomainModel> AddAsync(TDomainModel domainModel)
        {
            Validator.Save?.Validate(domainModel);
            return await ReadWriteRepository.AddAsync(domainModel);
        }

        public async Task AddRangeAsync(IEnumerable<TDomainModel> domainModels)
        {
            foreach (var domainModel in domainModels)
                Validator.Save?.Validate(domainModel);

            await ReadWriteRepository.AddRangeAsync(domainModels);
        }

        public ValueTask<TDomainModel> GetByIdAsync(TDomainId id)
        {
            return ReadWriteRepository.GetByIdAsync(id);
        }

        public ValueTask<TDomainModel> GetByIdAsync(TDomainId id, params string[] inflates)
        {
            throw new NotImplementedException();
        }

        public IAsyncEnumerable<TDomainModel> GetAllAsync()
        {
            return ReadWriteRepository.GetAllAsync();
        }

        public IAsyncEnumerable<TDomainModel> GetAllAsync(params string[] inflates)
        {
            return ReadWriteRepository.GetAllAsync(inflates);
        }

        public ValueTask<TDomainModel> Get(Expression<Func<TDomainModel, bool>> predicate, params string[] inflates)
        {
            return ReadWriteRepository.Get(predicate, inflates);
        }

        public ValueTask<int> Count(Expression<Func<TDomainModel, bool>> predicate = default)
        {
            return ReadWriteRepository.Count(predicate);
        }

        public async Task UpdateAsync(TDomainModel domainModel)
        {
            Validator.Update?.Validate(domainModel);
            await ReadWriteRepository.UpdateAsync(domainModel);
        }

        public async Task RemoveAsync(TDomainId id)
        {
            var entity = await GetByIdAsync(id);
            await RemoveAsync(entity);
        }

        public async Task RemoveAsync(TDomainModel domainModel)
        {
            Validator.Delete?.Validate(domainModel);
            await ReadWriteRepository.RemoveAsync(domainModel);
        }
    }
}