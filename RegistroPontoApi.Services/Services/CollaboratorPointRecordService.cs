using System;
using System.Threading.Tasks;
using AutoMapper;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroApi.Domain.ViewModels;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Services.Interfaces;

namespace RegistroPontoApi.Services.Services
{
    public class CollaboratorPointRecordService : BaseService<CollaboratorPointRecord, SimpleId<Guid>>,
        ICollaboratorPointRecordService
    {
        private readonly IMapper _mapper;

        public CollaboratorPointRecordService(IUnitOfWork unitOfWork,
            IServiceValidator<CollaboratorPointRecord> validator,
            IMapper mapper) : base(
            unitOfWork.CollaboratorPointRecordRepository,
            validator)
        {
            _mapper = mapper;
        }

        public async ValueTask<CollaboratorPointRecord> AddPointRecord(PointRecordInput pointRecordInput)
        {
            var collaboratorPoint = _mapper.Map<CollaboratorPointRecord>(pointRecordInput);
            return await ReadWriteRepository.AddAsync(collaboratorPoint);
        }
    }
}