using System;
using System.Linq;
using System.Threading.Tasks;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Services.Interfaces;

namespace RegistroPontoApi.Services.Services
{
    public class CollaboratorService : BaseService<Collaborator, SimpleId<Guid>>, ICollaboratorService
    {
        public CollaboratorService(IUnitOfWork unitOfWork,
            IServiceValidator<Collaborator> validator) : base(
            unitOfWork.CollaboratorRepository,
            validator)
        {
        }

        public async ValueTask<decimal> GetCollaboratorsTotalHours()
        {
            decimal sum = 0;
            await foreach (var c in ReadWriteRepository.GetAllAsync())
                sum = c.CollaboratorPointRecords.Select(s => (decimal) s.PointDateTime.TimeOfDay.TotalHours).Sum();

            return sum;
        }
    }
}