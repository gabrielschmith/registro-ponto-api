using System;
using System.Threading.Tasks;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.User;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Services.Interfaces;

namespace RegistroPontoApi.Services.Services
{
    public class ViewerService : BaseService<Viewer, SimpleId<Guid>>, IViewerService
    {
        public ViewerService(IUnitOfWork unitOfWork,
            IServiceValidator<Viewer> validator) : base(
            unitOfWork.ViewerRepository,
            validator)
        {
        }

        public async ValueTask<Viewer> Authenticate(string username, string password)
        {
            var viewer = await ReadWriteRepository.Get(w =>
                w.Username == username && w.Password == password);

            if (viewer == default)
                return default;

            viewer.Password = string.Empty;
            return viewer;
        }
    }
}