using Microsoft.Extensions.DependencyInjection;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroPontoApi.Infrastructure.CrossCutting.Validators.Interfaces;
using RegistroPontoApi.Services.Interfaces;
using RegistroPontoApi.Services.Services;
using RegistroPontoApi.Services.Validators;

namespace RegistroPontoApi.Services.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<ICollaboratorService, CollaboratorService>();
            services.AddScoped<ICollaboratorPointRecordService, CollaboratorPointRecordService>();
            services.AddScoped<IViewerService, ViewerService>();
            services.AddValidators();
            return services;
        }

        private static void AddValidators(this IServiceCollection services)
        {
            services.AddScoped(typeof(IServiceValidator<>), typeof(ServiceValidator<>));
            services.AddTransient<ISaveValidator<Collaborator>, CollaboratorValidator>();
            services.AddTransient<IUpdateValidator<Collaborator>, CollaboratorValidator>();
            services.AddTransient<ISaveValidator<CollaboratorPointRecord>, CollaboratorPointRecordValidator>();
            services.AddTransient<IUpdateValidator<CollaboratorPointRecord>, CollaboratorPointRecordValidator>();
        }
    }
}