using RegistroPontoApi.Infrastructure.CrossCutting.Validators.Interfaces;

namespace RegistroPontoApi.Services.Interfaces
{
    public interface IServiceValidator<TDomainModel>
    {
        ISaveValidator<TDomainModel> Save { get; set; }

        IUpdateValidator<TDomainModel> Update { get; set; }

        IDeleteValidator<TDomainModel> Delete { get; set; }
    }
}