using System;
using System.Threading.Tasks;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;

namespace RegistroPontoApi.Services.Interfaces
{
    public interface ICollaboratorService : IBaseService<Collaborator, SimpleId<Guid>>
    {
        ValueTask<decimal> GetCollaboratorsTotalHours();
    }
}