using RegistroPontoApi.Infrastructure.Repositories.Interfaces;

namespace RegistroPontoApi.Services.Interfaces
{
    public interface IBaseService<TDomainModel, in TDomainId> :
        IReadWriteRepository<TDomainModel, TDomainId> where TDomainModel : TDomainId
    {
    }
}