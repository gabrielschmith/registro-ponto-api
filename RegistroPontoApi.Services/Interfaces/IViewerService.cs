using System;
using System.Threading.Tasks;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.User;

namespace RegistroPontoApi.Services.Interfaces
{
    public interface IViewerService : IBaseService<Viewer, SimpleId<Guid>>
    {
        ValueTask<Viewer> Authenticate(string username, string password);
    }
}