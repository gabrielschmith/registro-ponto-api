using System;
using System.Threading.Tasks;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroApi.Domain.ViewModels;

namespace RegistroPontoApi.Services.Interfaces
{
    public interface ICollaboratorPointRecordService : IBaseService<CollaboratorPointRecord, SimpleId<Guid>>
    {
        ValueTask<CollaboratorPointRecord> AddPointRecord(PointRecordInput pointRecordInput);
    }
}