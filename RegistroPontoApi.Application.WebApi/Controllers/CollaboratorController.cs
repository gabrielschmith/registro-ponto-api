using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroApi.Domain.ViewModels;
using RegistroPontoApi.Application.WebApi.Resources;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Infrastructure.Repositories.Models;
using RegistroPontoApi.Services.Interfaces;

namespace RegistroPontoApi.Application.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class CollaboratorController : ControllerBase
    {
        private readonly ICollaboratorPointRecordService _collaboratorPointRecordService;

        private readonly ICollaboratorService _collaboratorService;
        private readonly IUnitOfWork _unitOfWork;

        public CollaboratorController(IUnitOfWork unitOfWork, ICollaboratorService collaboratorService,
            ICollaboratorPointRecordService collaboratorPointRecordService)
        {
            _unitOfWork = unitOfWork;
            _collaboratorService = collaboratorService;
            _collaboratorPointRecordService = collaboratorPointRecordService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IAsyncEnumerable<Collaborator> Get()
        {
            return _unitOfWork.CollaboratorRepository.GetAllAsync(nameof(CollaboratorModel.CollaboratorPointRecords));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await _collaboratorService.GetByIdAsync(new SimpleId<Guid>(id)));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(Collaborator collaborator)
        {
            var result = await _collaboratorService.AddAsync(collaborator);
            await _unitOfWork.CommitAsync();
            return Ok(result);
        }

        [HttpPut]
        [Authorize(Roles = "MANAGER")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(Collaborator collaborator)
        {
            await _collaboratorService.UpdateAsync(collaborator);
            await _unitOfWork.CommitAsync();
            return Ok(collaborator);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "MANAGER")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteById(Guid id)
        {
            var collaborator = await _collaboratorService.GetByIdAsync(new SimpleId<Guid>(id));
            await _collaboratorService.RemoveAsync(collaborator);
            await _unitOfWork.CommitAsync();
            return Ok(collaborator);
        }

        [HttpPost("{id}/pointRecord")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostPointRecord(Guid id, [FromBody] PointRecordInput pointRecordInput)
        {
            var collaborator = await _collaboratorService.GetByIdAsync(new SimpleId<Guid>(id));
            if (collaborator == default)
                return NotFound(new {message = Messages.CollaboratorInvalid});

            pointRecordInput.CollaboratorId = collaborator.Id;
            var entity = await _collaboratorPointRecordService.AddPointRecord(pointRecordInput);
            await _unitOfWork.CommitAsync();
            return Ok(entity);
        }

        [HttpGet("count")]
        [Authorize(Roles = "MANAGER")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCollaboratorsCount()
        {
            return Ok(await _collaboratorService.Count());
        }

        [HttpGet("count/totalHours")]
        [Authorize(Roles = "MANAGER")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCollaboratorsHoursCount()
        {
            return Ok(await _collaboratorService.GetCollaboratorsTotalHours());
        }
    }
}