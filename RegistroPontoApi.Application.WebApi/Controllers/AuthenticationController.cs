using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RegistroApi.Domain.ViewModels;
using RegistroPontoApi.Application.WebApi.Resources;
using RegistroPontoApi.Infrastructure.Authentication.Interfaces;
using RegistroPontoApi.Services.Interfaces;

namespace RegistroPontoApi.Application.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly ITokenService _tokenService;
        private readonly IViewerService _viewerService;

        public AuthenticationController(IViewerService viewerService,
            ITokenService tokenService)
        {
            _viewerService = viewerService;
            _tokenService = tokenService;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<dynamic>> Authenticate(AuthenticationInput authenticationInput)
        {
            var viewer = await _viewerService.Authenticate(authenticationInput.Username, authenticationInput.Password);

            if (viewer == default)
                return Unauthorized(new {message = Messages.UserInvalid});

            var token = _tokenService.GenerateToken(viewer);
            return new {viewer, token};
        }
    }
}