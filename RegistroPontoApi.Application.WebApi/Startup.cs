using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RegistroPontoApi.Application.WebApi.Extensions;
using RegistroPontoApi.Infrastructure.Authentication.Extensions;
using RegistroPontoApi.Infrastructure.Repositories;
using RegistroPontoApi.Infrastructure.Repositories.Extensions;
using RegistroPontoApi.Services.Extensions;

namespace RegistroPontoApi.Application.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCors()
                .AddMapper()
                .AddDatabaseProvider(Configuration)
                .AddAuthenticationProvider(Configuration)
                .AddRepositories()
                .AddServices()
                .AddSwagger()
                .AddControllers()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                );
            ;
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, Seeder seeder)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Registro Ponto V1"); });

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            seeder.SeedSupervisorUser().GetAwaiter().GetResult();
            seeder.SeedSimpleUser().GetAwaiter().GetResult();
        }
    }
}