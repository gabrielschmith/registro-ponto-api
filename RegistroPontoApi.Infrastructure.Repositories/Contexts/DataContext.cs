using Microsoft.EntityFrameworkCore;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Contexts
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<CollaboratorModel> Collaborator { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}