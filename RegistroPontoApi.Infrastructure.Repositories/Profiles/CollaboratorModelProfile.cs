using AutoMapper;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroApi.Domain.Entities.User;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Profiles
{
    public class CollaboratorModelProfile : Profile
    {
        public CollaboratorModelProfile()
        {
            CreateMap<Collaborator, CollaboratorModel>()
                .ForMember(d => d.Id, s => s.MapFrom(m => m.Id))
                .ForMember(d => d.Description, s => s.MapFrom(m => m.Name))
                .ForMember(d => d.Username, s => s.MapFrom(m => m.Username))
                .ForMember(d => d.Password, s => s.MapFrom(m => m.Password))
                .ForMember(d => d.Role, s => s.MapFrom(m => m.Role))
                .ForAllOtherMembers(d => d.Ignore());

            CreateMap<CollaboratorModel, Collaborator>()
                .ForMember(d => d.Id, s => s.MapFrom(m => m.Id))
                .ForMember(d => d.Name, s => s.MapFrom(m => m.Description))
                .ForMember(d => d.Username, s => s.MapFrom(m => m.Username))
                .ForMember(d => d.Password, s => s.MapFrom(m => m.Password))
                .ForMember(d => d.Role, s => s.MapFrom(m => m.Role))
                .ForMember(d => d.CollaboratorPointRecords, s => s.MapFrom(m => m.CollaboratorPointRecords))
                .ForAllOtherMembers(d => d.Ignore());

            CreateMap<Viewer, CollaboratorModel>()
                .ForMember(d => d.Id, s => s.MapFrom(m => m.Id))
                .ForMember(d => d.Username, s => s.MapFrom(m => m.Username))
                .ForMember(d => d.Password, s => s.MapFrom(m => m.Password))
                .ForMember(d => d.Role, s => s.MapFrom(m => m.Role))
                .ForAllOtherMembers(d => d.Ignore());

            CreateMap<CollaboratorModel, Viewer>()
                .ForMember(d => d.Id, s => s.MapFrom(m => m.Id))
                .ForMember(d => d.Username, s => s.MapFrom(m => m.Username))
                .ForMember(d => d.Password, s => s.MapFrom(m => m.Password))
                .ForMember(d => d.Role, s => s.MapFrom(m => m.Role))
                .ForAllOtherMembers(d => d.Ignore());
        }
    }
}