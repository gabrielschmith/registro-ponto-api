using AutoMapper;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroApi.Domain.ViewModels;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Profiles
{
    public class CollaboratorPointRecordModelProfile : Profile
    {
        public CollaboratorPointRecordModelProfile()
        {
            CreateMap<CollaboratorPointRecord, CollaboratorPointRecordModel>()
                .ForMember(d => d.Id, s => s.MapFrom(m => m.Id))
                .ForMember(d => d.CollaboratorId, s => s.MapFrom(m => m.CollaboratorId))
                .ForMember(d => d.PointDateTime, s => s.MapFrom(m => m.PointDateTime))
                .ForMember(d => d.Operation, s => s.MapFrom(m => m.Operation))
                .ForMember(d => d.Collaborator, s => s.MapFrom(m => m.Collaborator))
                .ForAllOtherMembers(d => d.Ignore());

            CreateMap<CollaboratorPointRecordModel, CollaboratorPointRecord>()
                .ForMember(d => d.Id, s => s.MapFrom(m => m.Id))
                .ForMember(d => d.CollaboratorId, s => s.MapFrom(m => m.CollaboratorId))
                .ForMember(d => d.PointDateTime, s => s.MapFrom(m => m.PointDateTime))
                .ForMember(d => d.Operation, s => s.MapFrom(m => m.Operation))
                .ForMember(d => d.Collaborator, s => s.MapFrom(m => m.Collaborator))
                .ForAllOtherMembers(d => d.Ignore());

            CreateMap<PointRecordInput, CollaboratorPointRecord>()
                .ForMember(d => d.CollaboratorId, s => s.MapFrom(m => m.CollaboratorId))
                .ForMember(d => d.PointDateTime, s => s.MapFrom(m => m.PointRecordDate))
                .ForMember(d => d.Operation, s => s.MapFrom(m => m.Operation))
                .ForAllOtherMembers(d => d.Ignore());
        }
    }
}