using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RegistroPontoApi.Infrastructure.Repositories.Contexts;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Infrastructure.Repositories.Repositories;

namespace RegistroPontoApi.Infrastructure.Repositories.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public const string DatabaseProvider = "DatabaseProvider";

        public const string DatabaseConnectionString = "DatabaseConnectionString";

        public const string DatabaseName = "DatabaseName";

        public static IDictionary<string, Func<IConfiguration, DbContextOptionsBuilder, DbContextOptionsBuilder>>
            DatabaseProviderLookup =
                new Dictionary<string, Func<IConfiguration, DbContextOptionsBuilder, DbContextOptionsBuilder>>
                {
                    {"", UseInMemoryDatabaseProvider},
                    {"InMemory", UseInMemoryDatabaseProvider},
                    {"SqlServer", UseSqlServerDatabaseProvider}
                };

        public static IServiceCollection AddDatabaseProvider(this IServiceCollection services,
            IConfiguration configuration)
        {
            var provider = configuration.GetValue<string>(DatabaseProvider);
            services.AddDbContext<DataContext>(opt => DatabaseProviderLookup[provider](configuration, opt));
            services.AddScoped<DataContext>();
            services.AddTransient<Seeder>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<ICollaboratorRepository, CollaboratorRepository>();
            services.AddScoped<ICollaboratorPointRecordRepository, CollaboratorPointRecordRepository>();
            services.AddScoped<IViewerRepository, ViewerRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            return services;
        }

        private static DbContextOptionsBuilder UseInMemoryDatabaseProvider(IConfiguration configuration,
            DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            return dbContextOptionsBuilder.UseInMemoryDatabase(configuration.GetValue<string>(DatabaseName));
        }

        private static DbContextOptionsBuilder UseSqlServerDatabaseProvider(IConfiguration configuration,
            DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            return dbContextOptionsBuilder.UseSqlServer(configuration.GetValue<string>(DatabaseConnectionString));
        }
    }
}