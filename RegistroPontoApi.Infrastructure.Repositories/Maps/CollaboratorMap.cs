using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Maps
{
    public class CollaboratorMap : IEntityTypeConfiguration<CollaboratorModel>
    {
        public void Configure(EntityTypeBuilder<CollaboratorModel> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("Collaborator");

            builder.Property(e => e.Id)
                .HasColumnName("CollId")
                .IsRequired();

            builder.Property(e => e.Description)
                .HasColumnName("CollDescription")
                .HasMaxLength(100)
                .IsRequired()
                .IsUnicode(false);

            builder.Property(e => e.Description)
                .HasColumnName("CollDescription")
                .HasMaxLength(100)
                .IsRequired()
                .IsUnicode(false);
        }
    }
}