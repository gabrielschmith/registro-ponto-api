using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Maps
{
    public class CollaboratorPointRecordMap : IEntityTypeConfiguration<CollaboratorPointRecordModel>
    {
        public void Configure(EntityTypeBuilder<CollaboratorPointRecordModel> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("CollaboratorPointRecord");

            builder.Property(e => e.Id)
                .HasColumnName("ColpId");

            builder.Property(e => e.CollaboratorId)
                .HasColumnName("CollId")
                .IsUnicode(false);

            builder.Property(e => e.PointDateTime)
                .HasColumnName("ColpPointDateTime")
                .IsUnicode(false);

            builder.Property(e => e.Operation)
                .HasColumnName("ColpOperation");

            builder.HasOne(n => n.Collaborator)
                .WithMany(d => d.CollaboratorPointRecords)
                .HasForeignKey(d => d.CollaboratorId);
        }
    }
}