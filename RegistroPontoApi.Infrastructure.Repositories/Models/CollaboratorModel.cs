using System;
using System.Collections.Generic;
using RegistroApi.Domain.Entities;

namespace RegistroPontoApi.Infrastructure.Repositories.Models
{
    public class CollaboratorModel : SimpleId<Guid>
    {
        public CollaboratorModel()
        {
            CollaboratorPointRecords = new HashSet<CollaboratorPointRecordModel>();
        }

        public string Description { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        public ICollection<CollaboratorPointRecordModel> CollaboratorPointRecords { get; set; }
    }
}