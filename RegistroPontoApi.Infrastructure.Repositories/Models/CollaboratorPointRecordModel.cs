using System;
using RegistroApi.Domain.Entities;

namespace RegistroPontoApi.Infrastructure.Repositories.Models
{
    public class CollaboratorPointRecordModel : SimpleId<Guid>
    {
        public Guid CollaboratorId { get; set; }

        public DateTimeOffset PointDateTime { get; set; }

        public char Operation { get; set; }

        public CollaboratorModel Collaborator { get; set; }
    }
}