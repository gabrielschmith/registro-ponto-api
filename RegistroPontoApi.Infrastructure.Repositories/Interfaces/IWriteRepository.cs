using System.Collections.Generic;
using System.Threading.Tasks;

namespace RegistroPontoApi.Infrastructure.Repositories.Interfaces
{
    public interface IWriteRepository<TDomainModel, in TId>
    {
        ValueTask<TDomainModel> AddAsync(TDomainModel domainModel);

        Task AddRangeAsync(IEnumerable<TDomainModel> domainModels);

        Task UpdateAsync(TDomainModel obj);

        Task RemoveAsync(TId id);

        Task RemoveAsync(TDomainModel obj);
    }
}