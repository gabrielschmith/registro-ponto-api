namespace RegistroPontoApi.Infrastructure.Repositories.Interfaces
{
    public interface IReadWriteRepository<TDomainModel, in TId> :
        IReadRepository<TDomainModel, TId>,
        IWriteRepository<TDomainModel, TId>
        where TDomainModel : TId
    {
    }
}