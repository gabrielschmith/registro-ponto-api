using System;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;

namespace RegistroPontoApi.Infrastructure.Repositories.Interfaces
{
    public interface ICollaboratorPointRecordRepository : IReadWriteRepository<CollaboratorPointRecord, SimpleId<Guid>>
    {
    }
}