using System.Threading.Tasks;

namespace RegistroPontoApi.Infrastructure.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        ICollaboratorRepository CollaboratorRepository { get; }

        ICollaboratorPointRecordRepository CollaboratorPointRecordRepository { get; }

        IViewerRepository ViewerRepository { get; set; }

        Task<int> CommitAsync();

        void RejectChanges();

        void Dispose();
    }
}