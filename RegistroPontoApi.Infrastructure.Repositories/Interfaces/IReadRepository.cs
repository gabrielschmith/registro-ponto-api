using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RegistroPontoApi.Infrastructure.Repositories.Interfaces
{
    public interface IReadRepository<TDomainModel, in TId> : IReadRepository<TDomainModel>
    {
        ValueTask<TDomainModel> GetByIdAsync(TId id);

        ValueTask<TDomainModel> GetByIdAsync(TId id, params string[] inflates);
    }

    public interface IReadRepository<TDomainModel>
    {
        IAsyncEnumerable<TDomainModel> GetAllAsync();

        IAsyncEnumerable<TDomainModel> GetAllAsync(params string[] inflates);

        ValueTask<TDomainModel> Get(Expression<Func<TDomainModel, bool>> predicate, params string[] inflates);

        ValueTask<int> Count(Expression<Func<TDomainModel, bool>> predicate = default);
    }
}