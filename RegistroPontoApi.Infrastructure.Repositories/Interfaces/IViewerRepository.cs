using System;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.User;

namespace RegistroPontoApi.Infrastructure.Repositories.Interfaces
{
    public interface IViewerRepository : IReadWriteRepository<Viewer, SimpleId<Guid>>
    {
    }
}