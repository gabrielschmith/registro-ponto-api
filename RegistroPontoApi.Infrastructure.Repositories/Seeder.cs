using System;
using System.Linq;
using System.Threading.Tasks;
using RegistroPontoApi.Infrastructure.Repositories.Contexts;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories
{
    public class Seeder
    {
        private readonly DataContext _context;

        public Seeder(DataContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task SeedSupervisorUser()
        {
            var collaborator = new CollaboratorModel
            {
                Id = Guid.NewGuid(),
                Description = "Supervisor",
                Username = "supervisor",
                Password = "123",
                Role = "MANAGER"
            };

            if (!_context.Collaborator.Any(r => r.Username == collaborator.Username))
            {
                await _context.Collaborator.AddAsync(collaborator);
                await _context.SaveChangesAsync();
            }
        }

        public async Task SeedSimpleUser()
        {
            var collaborator = new CollaboratorModel
            {
                Id = Guid.NewGuid(),
                Description = "Colaborador",
                Username = "colaborador",
                Password = "123",
                Role = "USER"
            };

            if (!_context.Collaborator.Any(r => r.Username == collaborator.Username))
            {
                await _context.Collaborator.AddAsync(collaborator);
                await _context.SaveChangesAsync();
            }
        }
    }
}