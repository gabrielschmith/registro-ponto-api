using System;
using AutoMapper;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroPontoApi.Infrastructure.Repositories.Contexts;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Repositories
{
    public class CollaboratorRepository : Repository<Collaborator, CollaboratorModel, SimpleId<Guid>, DataContext>,
        ICollaboratorRepository
    {
        public CollaboratorRepository(IMapper mapper, DataContext context) : base(mapper, context)
        {
        }
    }
}