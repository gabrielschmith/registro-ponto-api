using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RegistroPontoApi.Infrastructure.Repositories.Extensions;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;

namespace RegistroPontoApi.Infrastructure.Repositories.Repositories
{
    public abstract class Repository<TDomainModel, TModel, TDomainId, TDbContext> :
        IReadWriteRepository<TDomainModel, TDomainId>,
        IDisposable
        where TDomainModel : TDomainId
        where TModel : class, TDomainId
        where TDbContext : DbContext
    {
        private readonly TDbContext _context;

        private readonly DbSet<TModel> _dbSet;
        private readonly IMapper _mapper;

        protected Repository(IMapper mapper, TDbContext context)
        {
            _mapper = mapper;
            _context = context;
            _dbSet = context.Set<TModel>();
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public async ValueTask<TDomainModel> AddAsync(TDomainModel domainModel)
        {
            var entity = await _dbSet.AddAsync(_mapper.Map<TModel>(domainModel));
            return _mapper.Map<TDomainModel>(entity.Entity);
        }

        public Task AddRangeAsync(IEnumerable<TDomainModel> domainModels)
        {
            return _dbSet.AddRangeAsync(_mapper.Map<IEnumerable<TModel>>(domainModels));
        }

        public ValueTask<TDomainModel> GetByIdAsync(TDomainId id)
        {
            return GetByIdAsync(id, default);
        }

        public async ValueTask<TDomainModel> GetByIdAsync(TDomainId id, params string[] inflates)
        {
            var query = _dbSet.AsNoTracking();
            query = InflateQuery(inflates, query);

            var entity = await query.FirstOrDefaultAsync(BuildIdPredicate(id));
            return _mapper.Map<TDomainModel>(entity);
        }

        public async ValueTask<TDomainModel> Get(Expression<Func<TDomainModel, bool>> predicate,
            params string[] inflates)
        {
            var pr = _mapper.Map<Expression<Func<TModel, bool>>>(predicate);
            var query = _dbSet.AsNoTracking();
            query = InflateQuery(inflates, query);

            var entity = await query.FirstOrDefaultAsync(pr);
            return _mapper.Map<TDomainModel>(entity);
        }

        public async ValueTask<int> Count(Expression<Func<TDomainModel, bool>> predicate)
        {
            var pr = _mapper.Map<Expression<Func<TModel, bool>>>(predicate);
            var query = _dbSet.AsNoTracking();
            if (pr != default)
                query = query.Where(pr);

            return await query.CountAsync();
        }

        public IAsyncEnumerable<TDomainModel> GetAllAsync()
        {
            return GetAllAsync(default);
        }

        public async IAsyncEnumerable<TDomainModel> GetAllAsync(params string[] inflates)
        {
            var query = _dbSet.AsNoTracking();
            query = InflateQuery(inflates, query);

            await foreach (var entity in query.AsAsyncEnumerable())
                yield return _mapper.Map<TDomainModel>(entity);
        }

        public async Task UpdateAsync(TDomainModel obj)
        {
            var mapped = _mapper.Map<TModel>(obj);
            var ids = typeof(TDomainId).GetProperties()
                .ToDictionary(property => property.Name, property => property.GetValue(mapped));
            var id = (TDomainId) ChangeType(ids, typeof(TDomainId));

            var avoidingAttachedEntity = await _dbSet.AsNoTracking().FirstOrDefaultAsync(BuildIdPredicate(id));
            _context.Entry(avoidingAttachedEntity).State = EntityState.Detached;

            var entry = _context.Entry(mapped);
            if (entry.State == EntityState.Detached) _context.Attach(mapped);

            _context.Entry(mapped).State = EntityState.Modified;
        }

        public async Task RemoveAsync(TDomainId id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null) return;

            await RemoveAsync(entity);
        }

        public Task RemoveAsync(TDomainModel obj)
        {
            return Task.FromResult(_dbSet.Remove(_mapper.Map<TModel>(obj)));
        }

        public Task UpdateRangeAsync(IEnumerable<TDomainModel> entities)
        {
            _dbSet.UpdateRange(_mapper.Map<IEnumerable<TModel>>(entities));
            return Task.CompletedTask;
        }

        public Task RemoveRangeAsync(IEnumerable<TDomainModel> entities)
        {
            _dbSet.RemoveRange(_mapper.Map<IEnumerable<TModel>>(entities));
            return Task.CompletedTask;
        }

        private Expression<Func<TModel, bool>> BuildIdPredicate(TDomainId id)
        {
            Expression<Func<TModel, bool>> predicate = default;
            foreach (var property in typeof(TDomainId).GetProperties())
            {
                var value = property.GetValue(id);
                var parameter = Expression.Parameter(typeof(TModel), typeof(TModel).Name);
                var memberExpression = Expression.Property(parameter, property.Name);
                value = ChangeType(value, memberExpression.Type);
                var condition = Expression.Equal(memberExpression,
                    Expression.Convert(Expression.Constant(value), memberExpression.Type));
                var lambda = Expression.Lambda<Func<TModel, bool>>(condition, parameter);
                predicate = predicate?.AndAlso(lambda) ?? lambda;
            }

            return predicate;
        }

        private object ChangeType(object value, Type desiredType)
        {
            if (value != default)
                return _mapper.Map(value, value.GetType(), desiredType);

            if (desiredType.IsGenericType && desiredType.GetGenericTypeDefinition() == typeof(Nullable<>))
                return default;

            return desiredType.IsValueType ? Activator.CreateInstance(desiredType) : default;
        }

        private static IQueryable<TModel> InflateQuery(string[] inflates, IQueryable<TModel> query)
        {
            if (inflates != default && inflates.Any())
                inflates.ToList().ForEach(inflate => query = query.Include(inflate));

            return query;
        }
    }
}