using System;
using AutoMapper;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.User;
using RegistroPontoApi.Infrastructure.Repositories.Contexts;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Repositories
{
    public class ViewerRepository : Repository<Viewer, CollaboratorModel, SimpleId<Guid>, DataContext>,
        IViewerRepository
    {
        public ViewerRepository(IMapper mapper, DataContext context) : base(mapper, context)
        {
        }
    }
}