using System;
using AutoMapper;
using RegistroApi.Domain.Entities;
using RegistroApi.Domain.Entities.Collaborator;
using RegistroPontoApi.Infrastructure.Repositories.Contexts;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;
using RegistroPontoApi.Infrastructure.Repositories.Models;

namespace RegistroPontoApi.Infrastructure.Repositories.Repositories
{
    public class CollaboratorPointRecordRepository :
        Repository<CollaboratorPointRecord, CollaboratorPointRecordModel, SimpleId<Guid>, DataContext>,
        ICollaboratorPointRecordRepository
    {
        public CollaboratorPointRecordRepository(IMapper mapper, DataContext context) : base(mapper, context)
        {
        }
    }
}