using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RegistroPontoApi.Infrastructure.Repositories.Contexts;
using RegistroPontoApi.Infrastructure.Repositories.Interfaces;

namespace RegistroPontoApi.Infrastructure.Repositories.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public UnitOfWork(DataContext context,
            IServiceProvider serviceProvider)
        {
            _context = context;
            CollaboratorRepository = serviceProvider.GetRequiredService<ICollaboratorRepository>();
            CollaboratorPointRecordRepository =
                serviceProvider.GetRequiredService<ICollaboratorPointRecordRepository>();
            ViewerRepository = serviceProvider.GetRequiredService<IViewerRepository>();
        }

        public ICollaboratorRepository CollaboratorRepository { get; }

        public ICollaboratorPointRecordRepository CollaboratorPointRecordRepository { get; }

        public IViewerRepository ViewerRepository { get; set; }

        public Task<int> CommitAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void RejectChanges()
        {
            foreach (var entry in _context.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged))
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}