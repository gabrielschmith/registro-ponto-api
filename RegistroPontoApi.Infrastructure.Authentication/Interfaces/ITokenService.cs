using RegistroApi.Domain.Entities.User;

namespace RegistroPontoApi.Infrastructure.Authentication.Interfaces
{
    public interface ITokenService
    {
        string GenerateToken(Viewer viewer);
    }
}