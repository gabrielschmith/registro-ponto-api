using System;

namespace RegistroApi.Domain.Entities.Collaborator
{
    public class CollaboratorPointRecord : SimpleId<Guid>
    {
        public Guid CollaboratorId { get; set; }

        public DateTimeOffset PointDateTime { get; set; }

        public char Operation { get; set; }

        public Collaborator Collaborator { get; set; }
    }
}