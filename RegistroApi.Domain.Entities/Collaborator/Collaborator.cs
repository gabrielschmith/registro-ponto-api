using System;
using System.Collections.Generic;

namespace RegistroApi.Domain.Entities.Collaborator
{
    public class Collaborator : SimpleId<Guid>
    {
        public string Name { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        public ICollection<CollaboratorPointRecord> CollaboratorPointRecords { get; set; }
    }
}