using System;

namespace RegistroApi.Domain.Entities.User
{
    public class Viewer : SimpleId<Guid>
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }
    }
}