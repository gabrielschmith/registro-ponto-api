namespace RegistroApi.Domain.Entities
{
    public class SimpleId<T>
    {
        public SimpleId()
        {
        }

        public SimpleId(T id)
        {
            Id = id;
        }

        public T Id { get; set; }
    }
}