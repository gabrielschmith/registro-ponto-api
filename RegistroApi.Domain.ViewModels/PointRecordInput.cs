using System;

namespace RegistroApi.Domain.ViewModels
{
    public class PointRecordInput
    {
        public Guid CollaboratorId { get; set; }

        public DateTime PointRecordDate { get; set; }

        public char Operation { get; set; }
    }
}